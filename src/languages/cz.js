export const cz = {
    menu:{
        we:"ITCrows",
        about:"Co děláme",
        portfolio: "Portfolio",
        who: "Kdo za tím stojí",
        career: "Kariéra",
        contact: "Kontakt"
    },
    mainPage:{
        text: `My jsme ITcrows. Vrány sedící za počítačem.`,
        title: `Nadpis`
    },
    buttons:{
        contactUs: `Napište nám`
    }
    
}