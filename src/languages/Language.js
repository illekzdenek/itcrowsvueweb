import {cz} from "./cz";

export default function translate(key,lang){
    let keys = key.split(".")
    if (lang === "cz"){
        for(let section in cz){
            if (section === keys[0]){
                for(let translation in cz[section]){
                    if (translation === keys[1])
                    return cz[section][translation]
                }
            }
        }
    }
    
    return "!Překlad nenalezen!"
}

