import { References } from "./References"
import { Employees } from "./Employees"
import { Positions } from "./Positions"

export function getReferenceByIndex(index){
    if (References[index] !== undefined){
        return References[index]
    }
    return null
}
export function getAllreferences(){
    if (References.length>0){
        return References
    }
    return null
}
export function getNumberOfReferences(){
    return References.length
}
export function getAllEmployees(){
    if (Employees.length>0){
        return Employees
    }
    return null
}
export function getAllPositions(){
    if (Positions.length>0){
        return Positions
    }
    return null
}