export const References = [
    {
        name:"Take5",
        thumbnailImages:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        images:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in molestie nunc. Nunc id lectus ullamcorper orci ultrices accumsan et nec dolor. Pellentesque sed elit non ipsum posuere facilisis. In congue sem ac est eleifend mollis. Maecenas vestibulum volutpat tincidunt. Nam a nulla dignissim, tincidunt arcu in, tincidunt orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas non pharetra lectus, vel vulputate leo.",
        url: "https://take5.cz"
    },
    {
        name:"Recenzemakleru.cz",
        thumbnailImages:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        images:["recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        description: "Mega dobrá a dlouhá zakázka",
        url: "https://recenzemakleru.cz"
    },
    {
        name:"Recenzemakleru.cz2",
        thumbnailImages:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        images:["recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        description: "Mega dobrá a dlouhá zakázka",
        url: "https://recenzemakleru.cz"
    },
    {
        name:"Recenzemakleru.cz3",
        thumbnailImages:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        images:["recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        description: "Mega dobrá a dlouhá zakázka",
        url: "https://recenzemakleru.cz"
    },
    {
        name:"Take52",
        thumbnailImages:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        images:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in molestie nunc. Nunc id lectus ullamcorper orci ultrices accumsan et nec dolor. Pellentesque sed elit non ipsum posuere facilisis. In congue sem ac est eleifend mollis. Maecenas vestibulum volutpat tincidunt. Nam a nulla dignissim, tincidunt arcu in, tincidunt orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas non pharetra lectus, vel vulputate leo.",
        url: "https://take5.cz"
    },
    {
        name:"Take53",
        thumbnailImages:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        images:["take5/photo1.jpg","take5/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg","recenzemakleru/photo1.jpg","recenzemakleru/photo2.jpg"],
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in molestie nunc. Nunc id lectus ullamcorper orci ultrices accumsan et nec dolor. Pellentesque sed elit non ipsum posuere facilisis. In congue sem ac est eleifend mollis. Maecenas vestibulum volutpat tincidunt. Nam a nulla dignissim, tincidunt arcu in, tincidunt orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas non pharetra lectus, vel vulputate leo.",
        url: "https://take5.cz"
    }
]