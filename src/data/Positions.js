const requirements = {
    php: "PHP",
    wp: "Wordpress",
    css: "CSS",
    js: "Javascript",
    vue: "Vuejs",

}
export const Positions = [
    {
        title:"PHP developer",
        image:"take5/photo1.jpg",
        description: "Potřebujete funkční CRM, rezervační portál nebo vytvořit plugin na wordpress a dostupná řešení Vám již nestačí? Jsme schopni Vám vytvořit program přesně podle Vašich potřeb, který provážeme společně s Vaší bankou, fexterním systémem nebo propjíme s API.",
        requirements: [requirements.php, requirements.vue, requirements.css, requirements.wp]
    },
    {
        title:"PHP developer",
        image:"take5/photo1.jpg",
        description: "Potřebujete funkční CRM, rezervační portál nebo vytvořit plugin na wordpress a dostupná řešení Vám již nestačí? Jsme schopni Vám vytvořit program přesně podle Vašich potřeb, který provážeme společně s Vaší bankou, fexterním systémem nebo propjíme s API.",
        requirements: [requirements.php, requirements.vue, requirements.css, requirements.wp]
    },
    {
        title:"PHP developer",
        image:"take5/photo1.jpg",
        description: "Potřebujete funkční CRM, rezervační portál nebo vytvořit plugin na wordpress a dostupná řešení Vám již nestačí? Jsme schopni Vám vytvořit program přesně podle Vašich potřeb, který provážeme společně s Vaší bankou, fexterním systémem nebo propjíme s API.",
        requirements: [requirements.php, requirements.vue, requirements.css, requirements.wp]
    },
    {
        title:"PHP developer",
        image:"take5/photo1.jpg",
        description: "Potřebujete funkční CRM, rezervační portál nebo vytvořit plugin na wordpress a dostupná řešení Vám již nestačí? Jsme schopni Vám vytvořit program přesně podle Vašich potřeb, který provážeme společně s Vaší bankou, fexterním systémem nebo propjíme s API.",
        requirements: [requirements.php, requirements.vue, requirements.css, requirements.wp, requirements.vue, requirements.css, requirements.wp, requirements.vue, requirements.css, requirements.wp, requirements.vue, requirements.css, requirements.wp]
    },
]