export const MAIN_PAGE = "MAIN_PAGE"
export const ABOUT = "ABOUT"
export const PORTFOLIO = "PORTFOLIO"
export const WHO = "WHO"
export const CAREER = "CAREER"
export const CONTACT = "CONTACT"
export const DETAIL = "DETAIL"

export const MENU_STRUCTURE = [MAIN_PAGE, ABOUT, PORTFOLIO, WHO, CAREER, CONTACT]