import { createStore } from "vuex";
import * as appState from "../constants/appState";


export default createStore({
    state: {
        activePage: appState.MAIN_PAGE,
        activePageIndex: 0,
        mobileMenuOpen: false,
        reference:{
            referencePage:0,
            referenceIds:[0,1],
            thumbnails1:[],
            thumbnails2:[],
            activeDetailId: null
        },
        screen:{
            width:0
        }
    },
    mutations: {
        setActivePage(state, payload){
            state.activePage = payload
            //console.log(payload)
            if (payload === appState.DETAIL){
                state.activePageIndex = 2
            } else {
                state.activePageIndex = appState.MENU_STRUCTURE.indexOf(payload)
            }
        },
        plusActivePage(state){
            if(state.activePageIndex <5){
                state.activePageIndex = state.activePageIndex+1;
                state.activePage = appState.MENU_STRUCTURE[state.activePageIndex]
            }
            
        },
        minusActivePage(state){
            if(state.activePageIndex >0){
                state.activePageIndex = state.activePageIndex-1;
                state.activePage = appState.MENU_STRUCTURE[state.activePageIndex]
            }
        },
        openMobileMenu(state){
            state.mobileMenuOpen = !state.mobileMenuOpen
        },
        setReferencePage(state, payload){
            state.reference.referencePage = payload
            //console.log(payload)
            switch (payload) {
                case 0:
                    state.reference.referenceIds = [0,1]
                    break;
            
                case 1:
                    state.reference.referenceIds = [2,3]
                    break;
        
                case 2:
                    state.reference.referenceIds = [4,5]
                    break;
                
                default:
                    
                    state.reference.referenceIds = [0,1]
                    break;
            }
        },
        setActiveDetail(state,payload){
            state.reference.activeDetailId = payload
        },
        storeScreenWidth(state, payload){
            state.screen.width = payload
        }
    }, //synchronous
    actions: {}, //asynchronous
    modules: {},
    getters:{
        getActivePage(state){
            return state.activePage;
        },
        getActivePageIndex(state){
            return state.activePageIndex;
        },
        getMobileMenuOpen(state){
            return state.mobileMenuOpen;
        },
        getreference(state){
            return state.reference;
        },
        getActiveDetailId(state){
            return state.reference.activeDetailId
        },
        getScreen(state){
            return state.screen
        }
    }
});
