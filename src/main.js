import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";
import "./assets/tailwind.css";
//import axios from "axios";
//import VueAxios from "vue-axios"
//import * as api from "./constants/api"

createApp(App) //inicializace aplikace App
  .use(store)
  //.use(VueAxios,axios)
  .mount("#app");

  //Je server online?
/*axios.get(api.AUTH_SERVER_ONLINE).then((response) => {
  //console.log(response.data)
  store.commit("setServerState", response.data.online)
})*/